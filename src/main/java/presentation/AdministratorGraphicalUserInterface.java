package presentation;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;

public class AdministratorGraphicalUserInterface extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JScrollPane mainTablePane;
	private JButton btnNewMenuItem;
	private JButton btnNewCompositeItem;
	private JButton btnDeleteMenuItem;
	private JButton btnUpdateMenuItem;
	private JTable mainTable;
	private JTable secondaryTable;
	private JButton btnAdd;
	private JTextPane txtpnName;
	private JButton btnAddItemToComposite;
	private JPanel newItemPanel;
	private JScrollPane secondaryTablePane;
	
	/**
	 * Create the application.
	 */
	public AdministratorGraphicalUserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setBounds(100, 100, 538, 432);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblRestaurantAdministratorPanel = new JLabel("Restaurant Administrator Panel");
		lblRestaurantAdministratorPanel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblRestaurantAdministratorPanel.setBounds(135, 11, 243, 42);
		this.getContentPane().add(lblRestaurantAdministratorPanel);
		
		mainTablePane = new JScrollPane();
		mainTablePane.setBounds(10, 57, 338, 222);
		this.getContentPane().add(mainTablePane);
		
		DefaultTableModel modelTable1 = new DefaultTableModel(3,2);
		mainTable = new JTable(modelTable1);
		mainTablePane.setColumnHeaderView(mainTable);
		
		btnNewMenuItem = new JButton("New Base Menu Item");
		btnNewMenuItem.setBounds(358, 222, 154, 23);
		this.getContentPane().add(btnNewMenuItem);
		
		btnDeleteMenuItem = new JButton("Delete Menu Item");
		btnDeleteMenuItem.setBounds(358, 325, 154, 23);
		this.getContentPane().add(btnDeleteMenuItem);
		
		btnUpdateMenuItem = new JButton("Update Menu Item");
		btnUpdateMenuItem.setBounds(358, 359, 154, 23);
		this.getContentPane().add(btnUpdateMenuItem);
		
		btnNewCompositeItem = new JButton("New Composite Item");
		btnNewCompositeItem.setBounds(358, 256, 154, 23);
		this.getContentPane().add(btnNewCompositeItem);
		
		btnAddItemToComposite = new JButton("Add Item to Composite");
		btnAddItemToComposite.setBounds(358, 291, 154, 23);
		this.getContentPane().add(btnAddItemToComposite);
		
		newItemPanel = new JPanel();
		newItemPanel.setBounds(10, 286, 336, 96);
		this.getContentPane().add(newItemPanel);
		newItemPanel.setLayout(null);
		
		btnAdd = new JButton("Add");
		btnAdd.setBounds(277, 62, 59, 23);
		newItemPanel.add(btnAdd);
		
		txtpnName = new JTextPane();
		txtpnName.setText("Name");
		txtpnName.setBounds(144, 65, 114, 20);
		newItemPanel.add(txtpnName);
		newItemPanel.setVisible(false);
		
		secondaryTablePane = new JScrollPane();
		secondaryTablePane.setBounds(0, 0, 336, 92);
		//newItemPanel.add(secondaryTablePane);
		
		DefaultTableModel modelTable2 = new DefaultTableModel();
		secondaryTable = new JTable(modelTable2);
		secondaryTablePane.setViewportView(secondaryTable);
		secondaryTablePane.setVisible(false);
	}

	public JScrollPane getMainTablePane() {
		return mainTablePane;
	}

	public void setMainTablePane(JScrollPane mainTablePane) {
		this.mainTablePane = mainTablePane;
	}

	public JScrollPane getSecondaryTablePane() {
		return secondaryTablePane;
	}
	
	public JTable getMainTable() {
		return mainTable;
	}
	public void setMainTable(JTable table) {
		mainTable = table;
	}
	public JTable getSecondaryTable() {
		return secondaryTable;
	}
	public void setSecondaryTable(JTable table) {
		secondaryTable = table;
	}
	public void setSecondaryTablePane(JScrollPane secondaryTablePane) {
		this.secondaryTablePane = secondaryTablePane;
	}
	void btnNewMenuItemActionListener(final ActionListener actionListener) {
		btnNewMenuItem.addActionListener(actionListener);
	}
	void btnNewCompositeItemActionListener(final ActionListener actionListener) {
		btnNewCompositeItem.addActionListener(actionListener);
	}
	void btnDeleteMenuItemActionListener(final ActionListener actionListener) {
		btnDeleteMenuItem.addActionListener(actionListener);
	}
	void btnUpdateMenuItemActionListener(final ActionListener actionListener) {
		btnUpdateMenuItem.addActionListener(actionListener);
	}
	void btnAddItemToCompositeActionListener(final ActionListener actionListener) {
		btnAddItemToComposite.addActionListener(actionListener);
	}
	void btnAddActionListener(final ActionListener actionListener) {
		btnAdd.addActionListener(actionListener);
	}
	void setVisibleTablePane(boolean maybe) {
		secondaryTablePane.setVisible(maybe);
	}
	void setVisibleNewItem(boolean maybe) {
		newItemPanel.setVisible(maybe);
	}
	void initNewItem() {
		txtpnName.setText("Name");
	}
	public String getName() {
		return txtpnName.getText();
	}
}
