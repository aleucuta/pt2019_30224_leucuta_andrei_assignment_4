package presentation;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;

import model.MenuItem;

@SuppressWarnings("deprecation")
public class ChefGraphicalUserInterface extends JFrame implements Observer{

	private static final long serialVersionUID = 1L;
	private JScrollPane mainTablePane;
	private JButton btnDetails;
	private JButton btnDelete;
	private JTable mainTable;
	private JScrollPane secondaryTablePane;
	private JTable secondaryTable;
	
	public ChefGraphicalUserInterface() {
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setBounds(100, 100, 301, 432);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblWaiterGUI = new JLabel("Restaurant Chef Panel");
		lblWaiterGUI.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblWaiterGUI.setBounds(47, 11, 243, 42);
		this.getContentPane().add(lblWaiterGUI);
		
		mainTablePane = new JScrollPane();
		mainTablePane.setBounds(10, 57, 186, 222);
		this.getContentPane().add(mainTablePane);
		
		DefaultTableModel modelTable1 = new DefaultTableModel(null,new String[] {"Products to be made"});
		mainTable = new JTable(modelTable1);
		mainTablePane.setViewportView(mainTable);
		
		btnDetails = new JButton("Details");
		btnDetails.setBounds(206, 64, 76, 23);
		this.getContentPane().add(btnDetails);

		btnDelete = new JButton("Delete");
		btnDelete.setBounds(206, 98, 76, 23);
		this.getContentPane().add(btnDelete);

		secondaryTablePane = new JScrollPane();
		secondaryTablePane.setBounds(10, 290, 186, 91);
		getContentPane().add(secondaryTablePane);
		
		DefaultTableModel model = new DefaultTableModel(null,new String[] {"Details"});
		secondaryTable = new JTable(model);
		secondaryTablePane.setViewportView(secondaryTable);
	}

	public JScrollPane getMainTablePane() {
		return mainTablePane;
	}

	public void setMainTablePane(JScrollPane mainTablePane) {
		this.mainTablePane = mainTablePane;
	}


	public JTable getMainTable() {
		return mainTable;
	}
	public void setMainTable(JTable table) {
		mainTable = table;
	}

	void btnDetailsActionListener(final ActionListener actionListener) {
		btnDetails.addActionListener(actionListener);
	}
	
	void btnDeleteActionListener(final ActionListener actionListener) {
		btnDelete.addActionListener(actionListener);
	}
	
	public void setSecondaryTable(JTable table) {
		secondaryTable = table;
	}
	public JTable getSecondaryTable() {
		return secondaryTable;
	}
	public JScrollPane getSecondaryTablePane() {
		return secondaryTablePane;
	}
	
	public void update(Observable restaurant, Object menuItem) {
		System.out.println(menuItem);
		((Restaurant)restaurant).updateChefTable((MenuItem)menuItem);
	}

}
