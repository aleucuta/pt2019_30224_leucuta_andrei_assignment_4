package presentation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.BaseProduct;
import model.CompositeProduct;
import model.MenuItem;
import model.Order;
import model.RestaurantProcessing;

/**
 * @invariant isWellFormed()
 * @author Andrew
 *
 */
@SuppressWarnings("deprecation")
public class Restaurant extends Observable implements RestaurantProcessing{
	/**
	 * 
	 */
	public enum STANCE{NEW_COMPOSITE_ITEM,OTHER};
	
	private HashMap<Order, HashMap<String, MenuItem>> orders;
	private Map<String,MenuItem> menu;
	private AdministratorGraphicalUserInterface adminGUI;
	private WaiterGraphicalUserInterface waiterGUI;
	private ChefGraphicalUserInterface chefGUI;
	
	public Restaurant(AdministratorGraphicalUserInterface adminGUI,WaiterGraphicalUserInterface waiterGUI,ChefGraphicalUserInterface chefGUI) {
		this.orders = new HashMap<Order,HashMap<String,MenuItem>>();
		this.menu = new HashMap<String,MenuItem>();
		this.adminGUI = adminGUI;
		this.waiterGUI = waiterGUI;
		this.chefGUI = chefGUI;
		addObserver(chefGUI);
		this.chefGUI.setVisible(true);
		this.adminGUI.setVisible(true);
		this.waiterGUI.setVisible(true);
	}
	
	/**
	 * gets an order by it's id
	 * @param id the order's id
	 * @return the order
	 */
	public Order getOrderById(int id) {
		for(Order each : orders.keySet()) {
			if(each.getOrderID() == id)
				return each;
		}
		return null;
	}
	
	/**
	 * method that ensures that the restaurant's structures (menu and orders) are well formed
	 * @return true if the structures are well formed, false otherwise
	 */
	protected boolean isWellFormed() {
		
		for(MenuItem each : menu.values()) {
			if(each == null)
				return false;
		}
		for(String each : menu.keySet()) {
			if(each == null)
				return false;
		}
		for(Order each : orders.keySet()) {
			if(each == null)
				return false;
		}
		for(HashMap<String,MenuItem> eachMap : orders.values()){
			for(String eachString : eachMap.keySet()) {
				if(eachString == null)
					return false;
			}
			for(MenuItem eachItem : eachMap.values()) {
				if(eachItem == null)
					return false;
			}
		}
		return true;
	}
	
	/**
	 * gets the products in an order 
	 * @param order the order 
	 * @return the products
	 */
	public HashMap<String,MenuItem> getOrderDetailsForOrder(Order order){
		return orders.get(order);
	}
	
	/**
	 * adds a new base menu item
	 */
	public void addNewBaseMenuItem(MenuItem newMenuItem) {
		assert(newMenuItem!=null);
		assert(menu.containsValue(newMenuItem) == false);
		assert(isWellFormed());
		int oldMenuSize= menu.size();
		
		this.menu.put(((BaseProduct)newMenuItem).getName(), newMenuItem);
		
		assert(menu.size() == oldMenuSize+1);
		assert(isWellFormed());
	}
	
	/**
	 * finds an item by it's name
	 * @param name the name
	 * @return the item
	 */
	public MenuItem findItemByName(String name) {
		return menu.get(name);
	}
	
	/**
	 * deletes an item from a composite product by it's name
	 * @param compositeProductName the product
	 * @param name the name
	 */
	public void deleteFromCompositeByName(String compositeProductName,String name) {
		((CompositeProduct)menu.get(compositeProductName)).getItems().remove(menu.get(name));
	}
	
	/**
	 * creates a new composite item
	 * @param newMenuItem the item
	 */
	public void addNewCompositeMenuItem(MenuItem newMenuItem) {
		assert(newMenuItem!=null);
		assert(menu.containsValue(newMenuItem) == false);
		assert(isWellFormed());
		int oldMenuSize= menu.size();
		
		this.menu.put(((CompositeProduct)newMenuItem).getName(),newMenuItem);
		
		assert(menu.size() == oldMenuSize+1);
		assert(isWellFormed());
	}
	
	/**
	 * deletes an item from the menu
	 * @param name the name of the to be deleted item
	 */
	public void deleteMenuItem(String name) {
		assert(name!=null);
		assert(menu.size()>0);
		assert(menu.containsKey(name));
		assert(isWellFormed());
		int oldMenuSize = menu.size();
		
		this.menu.remove(name);
		
		assert(isWellFormed());
		assert(menu.size()==oldMenuSize-1);
	}
	
	/**
	 * updates a menu item by it's name
	 * @param name the name of the item to be updated
	 * @param menuItemNewData the item's new data
	 */
	public void updateMenuItem(String name, MenuItem menuItemNewData) {
		assert(name!=null);
		assert(menu.containsKey(name));
		assert(menuItemNewData!=null);
		assert(isWellFormed());
		
		if(menuItemNewData instanceof CompositeProduct) {
			if(((CompositeProduct)menuItemNewData).getName().equals(name) == true) {
				menu.replace(name, menuItemNewData);
			}
			else {
				menu.remove(name);
				menu.put(((CompositeProduct)menuItemNewData).getName(),menuItemNewData);
			}
		}else 
		if(menuItemNewData instanceof BaseProduct){
			if(((BaseProduct)menuItemNewData).getName().equals(name) == true) {
				menu.replace(name, menuItemNewData);
			}
			else {
				menu.remove(name);
				menu.put(((BaseProduct)menuItemNewData).getName(),menuItemNewData);
			}
		}
		assert(isWellFormed());
	}
	/**
	 * creates a new order for a table
	 * @param table the table
	 */
	public void createNewOrder(int table) {
		assert(table>=0);
		int oldOrderSize = orders.size();
		
		Order order = new Order(new Date(),table);
		orders.put(order, new HashMap<String,MenuItem>());
		
		assert(orders.size() == oldOrderSize+1);
	}
	
	/**
	 * adds a new item to an order
	 * @param item the new item that will be added
	 * @param order the order
	 */
	public void addItemToOrder(MenuItem item, Order order) {
		orders.get(order).put(item.getName(),item);
		System.out.println(item);
		setChanged();
		notifyObservers(item);
	}
	
	/**
	 * computes the price for an order
	 * @param order the order
	 */
	public float computePriceForOrder(Order order) {
		assert(order!=null);
		assert(isWellFormed());
		assert(orders.keySet().contains(order));
		
		float price = 0;
		Collection<MenuItem> details = orders.get(order).values();
		for(MenuItem each : details) {
			price+=each.computePrice();
		}
		assert(price>=0);
		return price;	
	}
	
	/**
	 * generates the bill for an order
	 * @param order the order
	 * @return a list of strings containing the bill lines of text
	 */
	public List<String> generateBill(Order order) {
		assert(order!=null);
		assert(isWellFormed());
		assert(orders.keySet().contains(order));
		
		Map<String,MenuItem> orderDetails = getOrders().get(order);
		List<String> invoiceData = new ArrayList<String>();
		
		invoiceData.add(new String("\tRestaurant Order Invoice"));
		invoiceData.add(new String("--------------------------------------"));
		invoiceData.add(new String("Order ID : " + order.getOrderID()));
		invoiceData.add(new String("Date : " + order.getDate()));
		invoiceData.add(new String("Table : " + order.getTable()));
		invoiceData.add(new String("--------------------------------------"));
		
		for(MenuItem each : orderDetails.values()) {
			invoiceData.add(new String(" " + each.getName() + " .. " + each.computePrice()));
			invoiceData.add(new String(" "));
		}
		invoiceData.add(new String("--------------------------------------"));
		invoiceData.add(new String("TOTAL : " + computePriceForOrder(order)));
		invoiceData.add(new String("--------------------------------------"));
		invoiceData.add(new String("Thanks for eating at us!\nHope you have a great day!"));
		
		return invoiceData;
	}
	public void printMenuItems() {
		System.out.println(menu.toString());
	}
	public void printOrders() {
		System.err.println(orders.toString());
	}
	
	/**
	 * returns a map containing all the orders
	 * @return 
	 */
	public Map<Order,HashMap<String,MenuItem>> getOrders(){
		return this.orders;
	}
	
	/**
	 * @return a map containing the menu
	 */
	public Map<String,MenuItem> getMenu(){
		return this.menu;
	}
	
	/**
	 * takes a collection of menu items and composes a String[][] with the data
	 * @param menuCollection the menu items collection
	 * @return a String[][] containing the menu item collection's data
	 */
	public String[][] menuToString(Collection<MenuItem> menuCollection){
		String[][] data = new String[menu.size()][2];
		int i=0;
		for(MenuItem each : menuCollection) {
			data[i] = new String[2];
			data[i][0] = each.getName();
			data[i][1] = String.valueOf(each.computePrice());
			i++;
		}
		return data;
	}
	
	/**
	 * takes a collection of menu items and composes a String[][] with the names
	 * @param menuCollection the menu items collection
	 * @return a String[][] containing the menu item collection's names
	 */
	public String[][] productsToString(Collection<MenuItem> menuCollection){
		String[][] data = new String[menu.size()][1];
		int i=0;
		for(MenuItem each : menuCollection) {
			data[i] = new String[1];
			data[i][0] = each.getName();
			i++;
		}
		return data;
	}
	/**
	 * 
	 * takes a collection of orders and composes a String[][] with the data
	 * @param ordersDetails the order collection
	 * @return a String[][] containing the order collection's data
	 */
	public String[][] ordersToString(Set<Order> ordersDetails){
		
		String[][] data = new String[orders.size()][4];
		int i=0;
		for(Order each : ordersDetails) {
			data[i] = new String[4];
			data[i][0] = String.valueOf(each.getOrderID());
			data[i][1] = String.valueOf(each.getDate());
			data[i][2] = String.valueOf(each.getTable());
			data[i][3] = String.valueOf(this.orders.get(each).size());
			i++;
		}
		return data;
	}
	
	public void setOrders(HashMap<Order,HashMap<String,MenuItem>> orders) {
		this.orders = orders;
	}
	
	public void setMenu(Map<String,MenuItem> menu) {
		this.menu = menu;
	}
	/**
	 * updates chef table with a new item
	 * @param item the item
	 */
	public void updateChefTable(MenuItem item) {
		JTable mainTable = chefGUI.getMainTable();
		DefaultTableModel model = (DefaultTableModel)(mainTable.getModel());
		model.addRow(new String[] {item.getName()});
	}
	

}
