package presentation;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.BaseProduct;
import model.CompositeProduct;
import model.MenuItem;
import model.Order;
import application.Start;
import presentation.Restaurant.STANCE;

public class TableController {
	private Restaurant restaurant;
	private AdministratorGraphicalUserInterface adminGUI;
	private WaiterGraphicalUserInterface waiterGUI;
	private ChefGraphicalUserInterface chefGUI;
	private CompositeProduct currentCompositeProduct;
	private Order currentOrder;
	
	
	/**
	 * creates a table controller having a restaurant, and all the required GUIs
	 * @param restaurant the restaurant of the new controller
	 * @param adminGUI the adminGUI of the new controller
	 * @param waiterGUI the waiterGUI of the new controller
	 * @param chefGUI the chefGUI of the new controller
	 */
	public TableController(Restaurant restaurant,AdministratorGraphicalUserInterface adminGUI,WaiterGraphicalUserInterface waiterGUI,ChefGraphicalUserInterface chefGUI) {
		this.restaurant = restaurant;
		this.adminGUI = adminGUI;
		this.waiterGUI = waiterGUI;
		this.chefGUI = chefGUI;
		initializeActionListeners();
	}
	
	/**
	 * adds a new row to a table
	 * @param mainTable the table on which a new row will be added
	 */
	private void addNewRow(JTable mainTable) {
		DefaultTableModel model = (DefaultTableModel)(mainTable.getModel());
		int columnCount = mainTable.getColumnCount();
		String[] spaces = new String[columnCount];
		for(int i=0;i<columnCount;i++)
			spaces[i] = "";
		model.addRow(spaces);
	}
	
	/**
	 * initializes the action listeners of all GUIs
	 */
	private void initializeActionListeners() {
		refreshMainTable();
		refreshWaiterSecondaryTable();
		refreshMainWaiterTable();
		adminGUI.btnAddActionListener(btnAddActionListener());
		adminGUI.btnAddItemToCompositeActionListener(btnAddItemToCompositeActionListener());
		adminGUI.btnDeleteMenuItemActionListener(btnDeleteMenuItemActionListener());
		adminGUI.btnNewCompositeItemActionListener(btnNewCompositeItemActionListener());
		adminGUI.btnNewMenuItemActionListener(btnNewMenuItemActionListener());
		adminGUI.btnUpdateMenuItemActionListener(btnUpdateMenuItemActionListener());
		waiterGUI.btnAddActionListener(btnWaiterAddActionListener());
		waiterGUI.btnAddItemsToOrderActionListener(btnAddItemsToOrderActionListener());
		waiterGUI.btnGenerateInvoiceActionListener(btnGenerateInvoiceActionListener());
		waiterGUI.btnNewOrderActionListener(btnNewOrderActionListener());
		waiterGUI.btnUpdateOrderActionListener(btnUpdateOrderActionListener());
		chefGUI.btnDeleteActionListener(btnDeleteActionListener());
		chefGUI.btnDetailsActionListener(btnDetailsActionListener());
	}
	
	
	/**
	 * updates a menu item, be it a composite or a base menu item
	 * @param mainTable the table to fetch the information from
	 * @param restaurantStance the restaurant's stance
	 */
	private void updateItem(JTable mainTable,STANCE restaurantStance) {
		int selectedRowIndex = mainTable.getSelectedRow();
		String itemName = (String)(mainTable.getValueAt(selectedRowIndex, 0));
		MenuItem item = restaurant.findItemByName(itemName);
		if(item != null) {
			if(item instanceof BaseProduct) {
				restaurant.updateMenuItem(itemName, new BaseProduct(itemName,Float.valueOf((String)(mainTable.getValueAt(selectedRowIndex, 1)))));  
			}
			if(item instanceof CompositeProduct) {
				item.computePrice();
				refreshMainTable();
			}
		}
		else {
			if(item == null) {
				restaurant.addNewBaseMenuItem(new BaseProduct(itemName,Float.valueOf((String)mainTable.getValueAt(selectedRowIndex, 1))));
			}
		}
	}
	
	/**
	 * refreshes the main table from adminGUI
	 */
	private void refreshMainTable() {
		DefaultTableModel model = new DefaultTableModel(restaurant.menuToString(restaurant.getMenu().values()),new String[] {"Name","Price"});
		adminGUI.getMainTablePane().remove(adminGUI.getMainTable());
		JTable mainTable = new JTable(model);
		adminGUI.setMainTable(mainTable);
		adminGUI.getMainTablePane().setViewportView(mainTable);
		adminGUI.getMainTablePane().revalidate();
	}
	/**
	 * refreshes the main table from waiterGUI
	 */
	private void refreshMainWaiterTable() {
		DefaultTableModel model;
				//new DefaultTableModel(restaurant.ordersToString(restaurant.getOrders().keySet()),new String[] {"Id","Date","Table","Nr Items"});
		Set<Order> orders = restaurant.getOrders().keySet();
		model = new DefaultTableModel(restaurant.ordersToString(orders),new String[] {"Id","Date","Table","Nr Items"});
		
		waiterGUI.getMainTablePane().remove(waiterGUI.getMainTable());
		JTable mainTable = new JTable(model);
		waiterGUI.setMainTable(mainTable);
		waiterGUI.getMainTablePane().setViewportView(mainTable);
		waiterGUI.getMainTablePane().revalidate();
	}
	/**
	 * refreshes the secondary table from waiterGUI
	 */
	private void refreshWaiterSecondaryTable() {
		DefaultTableModel model = new DefaultTableModel(restaurant.productsToString(restaurant.getMenu().values()),new String[] {"Product"});
		waiterGUI.getSecondaryTablePane().remove(waiterGUI.getSecondaryTable());
		JTable secondaryTable = new JTable(model);
		waiterGUI.setSecondaryTable(secondaryTable);
		waiterGUI.getSecondaryTablePane().setViewportView(secondaryTable);
		waiterGUI.getSecondaryTablePane().revalidate();
	}
	
	/**
	 * creates an action listener that creates a new row for the main table in adminGUI
	 * @return the action listener
	 */
	private ActionListener btnNewMenuItemActionListener() {
		return (e->{
			addNewRow(adminGUI.getMainTable());
		});
	}
	/**
	 * creates an action listener that adds a new composite item to the menu
	 * @return the action listener
	 */
	private ActionListener btnNewCompositeItemActionListener() {
		return (e->{
			JTable mainTable = adminGUI.getMainTable();
			String prodName = (String) mainTable.getValueAt(mainTable.getSelectedRow(), 0);
			restaurant.deleteMenuItem(prodName);
			restaurant.addNewCompositeMenuItem(new CompositeProduct(prodName+"*",new ArrayList<MenuItem>()));
			refreshMainTable();
			refreshWaiterSecondaryTable();
			Start.serializeMenu();
			//adminGUI.setVisibleNewItem(true);
		});
	}
	/**
	 * creates an action listener that deletes an item from the menu
	 * @return the action listener
	 */
	private ActionListener btnDeleteMenuItemActionListener() {
		return (e->{
			JTable mainTable = adminGUI.getMainTable();
			String name = (String)(mainTable.getValueAt(mainTable.getSelectedRow(), 0));
			restaurant.deleteMenuItem(name);
			refreshWaiterSecondaryTable();
			refreshMainTable();
			Start.serializeMenu();
		});
	}
	/**
	 * creates an action listener that updates an item in the menu
	 * @return the action listener
	 */
	private ActionListener btnUpdateMenuItemActionListener() {
		return (e->{
			updateItem(adminGUI.getMainTable(), STANCE.OTHER);
			Start.serializeMenu();
			refreshWaiterSecondaryTable();
		});
	}
	
	/**
	 * creates an action listener that adds a composite item to the menu
	 * @return the action listener
	 */
	private ActionListener btnAddItemToCompositeActionListener() {
		return (e->{
			adminGUI.setVisibleNewItem(true);
			JTable mainTable = adminGUI.getMainTable();
			String prodName = (String) mainTable.getValueAt(mainTable.getSelectedRow(), 0);
			currentCompositeProduct = (CompositeProduct)restaurant.findItemByName(prodName);
		});
	}
	/**
	 * creates an action listener that adds a product to a composite product
	 * @return the action listener
	 */
	private ActionListener btnAddActionListener() {
		return (e->{
			//trebuie sa existe
			MenuItem currentItem = restaurant.findItemByName(adminGUI.getName());
			currentCompositeProduct.addToItems(currentItem);
			adminGUI.initNewItem();
			refreshMainTable();
			refreshWaiterSecondaryTable();
			Start.serializeMenu();
			
		});
	}
	/**
	 * creates an action listener that adds a new row to the waiterGUI main table
	 * @return the action listener
	 */
	private ActionListener btnNewOrderActionListener() {
		return(e->{
			addNewRow(waiterGUI.getMainTable());
		});
	}
	
	/**
	 * creates an action listener that prepares an order as 'currentOrder' for products to be added into
	 * @return the action listener
	 */
	private ActionListener btnAddItemsToOrderActionListener() {
		return(e->{
			waiterGUI.setVisibleNewItem(true);
			JTable mainTable = waiterGUI.getMainTable();
			int selectedOrderId = Integer.valueOf((String)mainTable.getValueAt(mainTable.getSelectedRow(), 0));
			currentOrder = restaurant.getOrderById(selectedOrderId);
			
		});
	}
	
	/**
	 * creates an action listener that adds a new item to an order that was prepared as 'currentOrder' 
	 * @return the action listener
	 */
	private ActionListener btnWaiterAddActionListener() {
		return(e->{
			MenuItem currentItem = restaurant.findItemByName(waiterGUI.getName());
			restaurant.addItemToOrder(currentItem, currentOrder);
			waiterGUI.initNewItem();
			refreshMainWaiterTable();
			Start.serializeOrders();
		});
	}
	/**
	 * creates an action listener that creates a new order
	 * @return the action listener
	 */
	private ActionListener btnUpdateOrderActionListener() {
		return(e->{
			JTable mainTable = waiterGUI.getMainTable();
			restaurant.createNewOrder(Integer.valueOf((String)mainTable.getValueAt(mainTable.getSelectedRow(), 2)));
			refreshMainWaiterTable();
		});
	}
	/**
	 * creates an action listener that generates an invoice for the selected order
	 * @return the action listener
	 */
	private ActionListener btnGenerateInvoiceActionListener() {
		return(e->{
			JTable mainTable = waiterGUI.getMainTable();
			int orderId = Integer.valueOf((String)mainTable.getValueAt(mainTable.getSelectedRow(), 0));
			Order order = restaurant.getOrderById(orderId);
			List<String> invoiceData = restaurant.generateBill(order);
			try {
				PrintWriter outputFile = new PrintWriter("invoice.txt","UTF-8");
				for(String each : invoiceData) {
					outputFile.println(each);
				}
				outputFile.close();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
	}
	
	/**
	 * creates an action listener that lists details about a product, so the chef knows how to make a product
	 * @return the action listener
	 */
	private ActionListener btnDetailsActionListener() {
		return(e->{
			JTable mainTable = chefGUI.getMainTable();
			String productName = (String)mainTable.getValueAt(mainTable.getSelectedRow(), 0);
			MenuItem item = restaurant.findItemByName(productName);
			if(item instanceof CompositeProduct) {
				DefaultTableModel model = new DefaultTableModel(restaurant.productsToString(((CompositeProduct)item).getItems()),new String[] {"Details"}); 
				JTable newTable = new JTable(model);
				chefGUI.setSecondaryTable(newTable);
				chefGUI.getSecondaryTablePane().setViewportView(newTable);
				chefGUI.getSecondaryTablePane().revalidate();
			}
		});
	}
	
	/**
	 * creates an action listener that deletes an item from the chef's to do list
	 * @return the action listener
	 */
	private ActionListener btnDeleteActionListener() {
		return(e->{
			JTable mainTable = chefGUI.getMainTable();
			DefaultTableModel model = (DefaultTableModel)mainTable.getModel();
			model.removeRow(mainTable.getSelectedRow());
		});
	}
	
}
