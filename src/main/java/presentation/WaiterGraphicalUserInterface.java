package presentation;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;

public class WaiterGraphicalUserInterface extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JScrollPane mainTablePane;
	private JButton btnNewOrder;
	private JButton btnGenerateInvoice;
	private JTable mainTable;
	private JButton btnAdd;
	private JTextPane txtpnName;
	private JButton btnAddItemsToOrder;
	private JPanel newItemPanel;
	private JButton btnUpdateOrder;
	private JScrollPane secondaryTablePane;
	private JTable secondaryTable;
	
	/**
	 * Create the application.
	 */
	public WaiterGraphicalUserInterface() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.setBounds(100, 100, 538, 432);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblWaiterGUI = new JLabel("Restaurant Waiter Panel");
		lblWaiterGUI.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblWaiterGUI.setBounds(135, 11, 243, 42);
		this.getContentPane().add(lblWaiterGUI);
		
		mainTablePane = new JScrollPane();
		mainTablePane.setBounds(10, 57, 338, 222);
		this.getContentPane().add(mainTablePane);
		
		DefaultTableModel modelTable1 = new DefaultTableModel(0,4);
		mainTable = new JTable(modelTable1);
		mainTablePane.setViewportView(mainTable);
		
		btnNewOrder = new JButton("New Order");
		btnNewOrder.setBounds(358, 88, 154, 23);
		this.getContentPane().add(btnNewOrder);

		btnAddItemsToOrder = new JButton("Add Items to Order");
		btnAddItemsToOrder.setBounds(358, 158, 154, 23);
		this.getContentPane().add(btnAddItemsToOrder);
		
		
		btnGenerateInvoice = new JButton("Generate Invoice");
		btnGenerateInvoice.setBounds(358, 54, 154, 23);
		this.getContentPane().add(btnGenerateInvoice);
		
		btnUpdateOrder = new JButton("Update Order");
		btnUpdateOrder.setBounds(358, 124, 154, 23);
		getContentPane().add(btnUpdateOrder);
		
		
		newItemPanel = new JPanel();
		newItemPanel.setBounds(10, 286, 336, 96);
		this.getContentPane().add(newItemPanel);
		newItemPanel.setLayout(null);
		newItemPanel.setVisible(false);
		
		btnAdd = new JButton("Add");
		btnAdd.setBounds(277, 62, 59, 23);
		newItemPanel.add(btnAdd);
		
		txtpnName = new JTextPane();
		txtpnName.setText("Product Name");
		txtpnName.setBounds(141, 65, 114, 20);
		newItemPanel.add(txtpnName);
		
		secondaryTablePane = new JScrollPane();
		secondaryTablePane.setBounds(358, 230, 154, 152);
		getContentPane().add(secondaryTablePane);
		
		DefaultTableModel model = new DefaultTableModel(0,1);
		secondaryTable = new JTable(model);
		secondaryTablePane.setViewportView(secondaryTable);
	}

	public JScrollPane getMainTablePane() {
		return mainTablePane;
	}

	public void setMainTablePane(JScrollPane mainTablePane) {
		this.mainTablePane = mainTablePane;
	}


	public JTable getMainTable() {
		return mainTable;
	}
	public void setMainTable(JTable table) {
		mainTable = table;
	}

	void btnNewOrderActionListener(final ActionListener actionListener) {
		btnNewOrder.addActionListener(actionListener);
	}
	
	void btnGenerateInvoiceActionListener(final ActionListener actionListener) {
		btnGenerateInvoice.addActionListener(actionListener);
	}
	
	void btnAddItemsToOrderActionListener(final ActionListener actionListener) {
		btnAddItemsToOrder.addActionListener(actionListener);
	}
	void btnAddActionListener(final ActionListener actionListener) {
		btnAdd.addActionListener(actionListener);
	}
	void btnUpdateOrderActionListener(final ActionListener actionListener) {
		btnUpdateOrder.addActionListener(actionListener);
	}
	public void setSecondaryTable(JTable table) {
		secondaryTable = table;
	}
	public JTable getSecondaryTable() {
		return secondaryTable;
	}
	public JScrollPane getSecondaryTablePane() {
		return secondaryTablePane;
	}
	void setVisibleNewItem(boolean maybe) {
		newItemPanel.setVisible(maybe);
	}
	void initNewItem() {
		txtpnName.setText("Product Name");
	}
	public String getName() {
		return txtpnName.getText();
	}
}