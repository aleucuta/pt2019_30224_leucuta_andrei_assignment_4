package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.BaseProduct;
import model.MenuItem;
import model.Order;
import presentation.AdministratorGraphicalUserInterface;
import presentation.ChefGraphicalUserInterface;
import presentation.Restaurant;
import presentation.TableController;
import presentation.WaiterGraphicalUserInterface;

public class Start {
	
	private static Restaurant restaurant;
	private static TableController tableController;
	private static AdministratorGraphicalUserInterface adminGUI;
	private static WaiterGraphicalUserInterface waiterGUI;
	private static ChefGraphicalUserInterface chefGUI;
	
	public static void main(String[] args) {
		adminGUI = new AdministratorGraphicalUserInterface();
		waiterGUI = new WaiterGraphicalUserInterface();
		chefGUI = new ChefGraphicalUserInterface();
		restaurant = new Restaurant(adminGUI,waiterGUI,chefGUI);
		deserializeMenu();
		deserializeOrders();
		tableController = new TableController(restaurant,adminGUI,waiterGUI,chefGUI);
//		List<Order> orders = new ArrayList<Order>();
//		for(Order each : restaurant.getOrders().keySet()) {
//			orders.add(each);
//		}
//		for(Order each : orders) {
//			restaurant.getOrders().remove(each);
//		}
//		serializeOrders();
		//serializeMenu();
		//serializeOrders();
		
	}
	
	public static void serializeMenu() {
		try {
			FileOutputStream fileMenu = new FileOutputStream("menu.ser");
			ObjectOutputStream outMenu = new ObjectOutputStream(fileMenu);
			outMenu.writeObject(restaurant.getMenu());
			outMenu.close();
			fileMenu.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void serializeOrders() {
		try {
			FileOutputStream fileOrders = new FileOutputStream("orders.ser");
			ObjectOutputStream outOrders = new ObjectOutputStream(fileOrders);
			outOrders.writeObject(restaurant.getOrders());
			outOrders.close();
			fileOrders.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void deserializeMenu() {
		try {
			FileInputStream fileMenu = new FileInputStream("menu.ser");
			ObjectInputStream inMenu = new ObjectInputStream(fileMenu);
			@SuppressWarnings("unchecked")
			Map<String,MenuItem> menu = (Map<String,MenuItem>)inMenu.readObject();
			restaurant.setMenu(menu);
			inMenu.close();
			fileMenu.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void deserializeOrders() {
		try {
			FileInputStream fileOrders = new FileInputStream("orders.ser");
			ObjectInputStream inOrders = new ObjectInputStream(fileOrders);
			@SuppressWarnings("unchecked")
			HashMap<Order,HashMap<String,MenuItem>> orders = (HashMap<Order,HashMap<String,MenuItem>>)inOrders.readObject();
			restaurant.setOrders(orders);
			inOrders.close();
			fileOrders.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
