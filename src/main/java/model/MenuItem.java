package model;

public interface MenuItem{
	
	/**
	 * computes the price for a specific menu item, be it composite or not
	 * @return the computed price
	 */
	public float computePrice();
	
	/**
	 * gets the name of a menu item
	 * @return the name
	 */
	public String getName();
	
	/**
	 * calculates the hash code, essential for maps
	 * @return the hash code
	 */
	public int hashCode();
}
