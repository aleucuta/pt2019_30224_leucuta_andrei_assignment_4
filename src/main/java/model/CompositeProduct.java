package model;

import java.io.Serializable;
import java.util.List;

public class CompositeProduct implements MenuItem,Serializable{
	
	private String name;
	private List<MenuItem> items;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MenuItem> getItems() {
		return items;
	}

	public void setItems(List<MenuItem> items) {
		this.items = items;
	}

	public CompositeProduct(String name, List<MenuItem> items) {
		this.name = name;
		this.items = items;
	}
	
	public void addToItems(MenuItem item) {
		this.items.add(item);
	}
	
	public CompositeProduct() {
		
	}
	
	public float computePrice() {
		float price = 0;
		for(MenuItem each : items) {
			price+=each.computePrice();
		}
		return price;
	}
	
	public int hashCode() {
		final int POWER = 7;
		final int PRIME = 38407;
		int hashValue = 0;
		int currentPower = 1;
		char[] name = this.name.toCharArray();
		for(char c:name) {
			hashValue+=(c - 'A' + 1)*currentPower % PRIME;
			currentPower = (currentPower * POWER) % PRIME;
		}
		return hashValue;
	}
	
}
