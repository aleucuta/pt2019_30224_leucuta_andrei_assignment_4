package model;

import java.io.Serializable;

public class BaseProduct implements MenuItem,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String name;
	float price;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public BaseProduct() {
	}
	
	public BaseProduct(String name,float price) {
		this.name = name;
		this.price = price;
	}
	
	public float computePrice() {
		return price;
	}

	public int hashCode() {
		final int POWER = 7;
		final int PRIME = 38407;
		int hashValue = 0;
		int currentPower = 1;
		char[] name = this.name.toCharArray();
		for(char c:name) {
			hashValue+=(c - 'A' + 1)*currentPower % PRIME;
			currentPower = (currentPower * POWER) % PRIME;
		}
		return hashValue;
	}
	
}
